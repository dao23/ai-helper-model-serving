from dotenv import load_dotenv
from fastapi import FastAPI, File, UploadFile, HTTPException
import mlflow
import os
import pandas as pd
import sys

load_dotenv()

app = FastAPI()

class Model:
    def __init__(self, model_name, model_stage):
        self.model = mlflow.pyfunc.load_mode(f"models://{model_name}/{model_stage}")

    def predict(self, data):
        predictions = self.model.predict(data)
        return predictions
    
# create model
model_name = 'm_name'
model = Model(model_name, 'Staging')

@app.post("/invocations")
async def create_upload_file(file: UploadFile = File(...)):
    if file.filename.endswith(".csv"):
        with open(file.filename, "wb") as f:
            f.write(file.file.read())
        data = pd.read_csv(file.filename)
        os.remove(file.filename)
        return list(model.predict(data))
    else:
        raise HTTPException(status_code=400, detail="Invalid file format. Only CSV Files accepted.")
    
if os.getenv("AWS_ACCESS_KEY_ID") is None or os.getenv("AWS_SECRET_ACCESS_KEY") is None:
    sys.exit("Required env vars not found")